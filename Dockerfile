FROM node:buster-slim AS builder

WORKDIR /home

RUN apt-get update && \
    apt-get install -y git && \
    npm install -g @angular/cli

RUN git clone https://gitlab.utc.fr/gutc-project/frontend.git && \
    cd frontend && \
    ng update && \
    npm update && \
    ng build

RUN git clone https://gitlab.utc.fr/gutc-project/backend.git

FROM python:3.8-alpine

RUN adduser -D utchanger

WORKDIR /home/utchanger

# Copy frontend
COPY --from=builder /home/frontend/dist /var/www/utchange/
RUN chown -R utchanger:utchanger /var/www/utchange

# Copy backend
COPY --from=builder /home/backend .

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev

RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt

RUN venv/bin/pip install gunicorn==19.9.0

RUN chmod +x boot.sh

ENV FLASK_APP backend.py

RUN chown -R utchanger:utchanger ./
USER utchanger

EXPOSE 5555
ENTRYPOINT ["./boot.sh"]
