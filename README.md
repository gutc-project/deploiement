# Déploiement d'UTChange

Le Dockerfile présent à la racine de ce dépôt permet de compiler le frontend d'UTChange et de l'intégrer au backend. Pour construire cette image unique, il suffit de lancer la commande suivante :

```bash
$ docker build -t utchange .
```

Il faut ensuite créer les deux volumes utilisés pour conserver les photos des articles et la base de données :

```bash
$ docker volume create gutc
$ docker volume create gutc-db
```

Ensuite, on va copier le fichier de secrets et modifier les valeurs de test :

```bash
$ cp secrets/gutc-db.secrets.example secrets/gutc-db
```

Il ne reste plus qu'à ajuster le nom de domaine dans le `docker-compose.yml` en fonction de l'environnement, puis de lancer le déploiment :

```bash
$ docker-compose up -d
# On vérifie que tout se passe bien
$ docker-compose logs -f
```
